The different files provided under different subdirectories contain the files necessary to construct the drone attachment. The content of each file is summarized below.

## Assemblies

- `Camera_Assembly.zip`: TODO
- `PCB_Assembly.zip`: TODO

## Multi-Body

- `Camera_Enclosure.SLDDRW`: TODO
- `pcb_assembly.SLDDRW`: TODO

## Arduino

- `PHYS371_Group1_main.ino`: TODO
